package npm.totebag;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import SQLite.BookmarkHelper;


/**
 * A simple {@link Fragment} subclass.
 */
public class Bookmark_list extends Fragment {

    public ListView lvBookmark;
    protected BookmarkAdapter bAdapter;
    public List<Item_object> mBookmarkList;
    private Context mContext;
    private ProgressBar loading;
    private BookmarkHelper SQL;

    public Bookmark_list() {

        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View thisView = inflater.inflate(R.layout.fragment_bookmark_list, container, false);
        this.SQL = new BookmarkHelper(getContext());
        lvBookmark = (ListView) thisView.findViewById(R.id.listView_bookmark);
        mBookmarkList = new ArrayList<>();
        new getData().execute("");


        loading = (ProgressBar) thisView.findViewById(R.id.loadBookmark);
        lvBookmark.setClickable(true);

        lvBookmark.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getContext(), view.getTag() + " clicked", Toast.LENGTH_SHORT).show();
            }
        });
//        sample data

        return thisView;

    }

    public void updateView()
    {
        lvBookmark.setAdapter(bAdapter);



    }

    class getData extends AsyncTask<String,String,String> {
        Document doc;
        Item_object temp;
        @Override
        protected String doInBackground(String... params) {
            try{
                mBookmarkList = SQL.getAllBookmarks();

                bAdapter = new BookmarkAdapter(getContext(), mBookmarkList);

            }
            catch (Exception e)
            {
                Log.i(e.getMessage(),"Error Bos");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loading.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "data loaded", Toast.LENGTH_SHORT).show();
            updateView();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
    }
 }


