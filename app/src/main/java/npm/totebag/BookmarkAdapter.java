package npm.totebag;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import SQLite.BookmarkHelper;

/**
 * Created by deny on 5/15/16.
 */
class BookmarkAdapter extends BaseAdapter{

    private Context mcontext;
    private List<Item_object> mBookmarkList;
    private BookmarkHelper SQL;

//    constructor

    public BookmarkAdapter(Context mcontext, List<Item_object> mBookmarkLis) {
        this.mcontext = mcontext;
        this.mBookmarkList = mBookmarkLis;
        this.SQL = new BookmarkHelper(mcontext);

    }

    @Override
    public int getCount() {
        return mBookmarkList.size();
    }

    @Override
    public Object getItem(int position) {
        return mBookmarkList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = View.inflate(mcontext, R.layout.row_layout_list, null);

        ImageView ivItem = (ImageView) v.findViewById(R.id.pictBookmark);
        TextView tvTitle = (TextView) v.findViewById(R.id.titleBookmark);
        ImageView iconSourceB = (ImageView) v.findViewById(R.id.icon_source_b);
        TextView tvDateTime = (TextView) v.findViewById(R.id.timeBookmark);
        TextView tvPrice = (TextView) v.findViewById(R.id.priceBookmark);
        TextView tvNego = (TextView) v.findViewById(R.id.negoPriceBookmark);
        ImageButton bRemove = (ImageButton) v.findViewById(R.id.removeFromBookmark);

        Picasso.with(mcontext).load(mBookmarkList.get(position).getPictItem()).into(ivItem);
        tvTitle.setText(mBookmarkList.get(position).getTitle());
        iconSourceB.setImageResource(R.drawable.logo_olx);
        tvDateTime.setText(mBookmarkList.get(position).getDateTime());
        tvPrice.setText(mBookmarkList.get(position).getPrice());
        tvNego.setText(mBookmarkList.get(position).getNego());
        bRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQL.deleteBookmark(mBookmarkList.get(position).getId());
                Toast.makeText(mcontext, "remove", Toast.LENGTH_SHORT).show();
                mBookmarkList.remove(mBookmarkList.get(position));
                notifyDataSetChanged();
            }
        });

        v.setTag(mBookmarkList.get(position).getTitle());

        return v;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        Log.i("notify", "notifyDataSetChanged: ");
    }
}