package npm.totebag;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import SQLite.BookmarkHelper;

public class Item_list extends AppCompatActivity {

    public ListView lvItemSearch;
    protected ListAdapter lAdapter;
    public List<Item_object> mItemSearchList;
    private Context mContext;
    private ProgressBar loading;
    private String query;

    private FloatingActionMenu menuBlue;

    private List<FloatingActionMenu> menus = new ArrayList<>();
    private Handler mUiHandler = new Handler();

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        mItemSearchList.clear();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        Intent intent = getIntent();
        query = intent.getStringExtra("keyword");
        Log.d("URL", query);
        lvItemSearch = (ListView) findViewById(R.id.listViewItemSearch);
        mItemSearchList = new ArrayList<>();

        new getData().execute("");
        loading = (ProgressBar) findViewById(R.id.loadItemSearch);

        menuBlue = (FloatingActionMenu) findViewById(R.id.menu_blue);

        menus.add(menuBlue);

        menuBlue.setIconAnimated(false);
        menuBlue.hideMenuButton(false);

        int delay = 400;
        for (final FloatingActionMenu menu : menus) {
            mUiHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    menu.showMenuButton(true);
                }
            }, delay);
            delay += 150;
        }

    }

    public void updateView() {
        lvItemSearch.setAdapter(lAdapter);

        lvItemSearch.setClickable(false);
        lvItemSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(Item_list.this, view.getTag() + " clicked", Toast.LENGTH_SHORT).show();

//                Log.d("onitem", "onItemClick: " + position);
            }
        });

    }


    class getData extends AsyncTask<String, String, String> {
        Document doc;
        Item_object temp;

        @Override
        protected String doInBackground(String... params) {
            try {

                String URL = "http://m.olx.co.id/all-results/q-"+query;
                //Log.d("URL", URL);
                doc = Jsoup.connect(URL).get();

                //get OLX Name and Link
                Elements items = doc.select("li.bgfff");
                BookmarkHelper bookmarkSQL = new BookmarkHelper(getApplicationContext());

//                int idItem = 0;
                for (Element itemTemp : items) {
                    temp = new Item_object();

                    Element itemLi = itemTemp.child(0);

                    String itemPict = itemTemp.select("span[class=lt-tmb]").attr("style");
                    temp.setPictItem(itemPict);
//                    Log.i(itemPict,"image loaded");

                    Element itemDetail = itemLi.child(1);
                    Element childItemNameElem = itemDetail.child(0);
                    temp.setTitle(childItemNameElem.html());

                    Element childItemDate = itemDetail.child(1);
                    temp.setDateTime(childItemDate.html());

                    Element childItemPrice = itemDetail.child(3);
                    temp.setPrice(childItemPrice.html());


                /*
                String URL = "http://m.kaskus.co.id/search/fjb?q="+query;
                //Log.d("URL", URL);
                doc = Jsoup.connect(URL).get();

                //get Kaskus Name and Link
                Elements items = doc.select("div.icon");
                BookmarkHelper bookmarkSQL = new BookmarkHelper(getApplicationContext());

                int idItem = 0;
                for (Element itemTemp : items) {
                    temp = new Item_object();
                    Element itemDetail = itemTemp.nextElementSibling();
                    Element itemNameElem = itemDetail.child(0);
                    temp.setLinkItem(itemNameElem.attr("href"));
                    //Log.i(itemNameElem.attr("href"),"item link kaskus");

                    Element itemDiv = itemTemp.child(0);
                    Element item = itemDiv.child(0);
                    temp.setPictItem(item.attr("data-src"));
                    temp.setTitle(item.attr("alt"));
//                    Log.i(item.attr("src"), "item image kaskus");
//                    Log.i(item.attr("alt"),"item name kaskus");

                    Element itemPrice = itemDetail.child(2);
                    Element childItemPrice = itemPrice.child(0);
                    temp.setPrice(childItemPrice.html());
//                    Log.i(childItemPrice.html(),"item Price kaskus");

                    Element itemCalendar = itemDetail.child(3);
                    Element childItemCal = itemCalendar.child(0);
                    temp.setDateTime(childItemCal.html());
//                    Log.i(childItemCal.html(),"item Calendar kaskus");

                    */

//                    temp.setId(idItem);
//                    idItem++;

                    //bookmarkSQL.insert(temp);
                    mItemSearchList.add(temp);
                }

                lAdapter = new ItemAdapter(getApplicationContext(),mItemSearchList);


            } catch (Exception e) {
                Log.i(e.getMessage(), "Error Bos");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

//            loading spinner
            loading.setVisibility(View.GONE);

            updateView();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
    }

}
