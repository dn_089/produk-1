package npm.totebag;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class Search extends Fragment {


    public Search() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search, container, false);

        Button searchButton = (Button) v.findViewById(R.id.searchItem);
        final EditText searchQuery = (EditText) v.findViewById(R.id.keyword);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String query    = searchQuery.getText().toString();

                Intent intent = new Intent(getActivity(),Item_list.class);
                intent.putExtra("keyword", query);
                intent.putExtra("secondKeyName", "SecondKeyValue");
                startActivity(intent);
            }
        });

        return v;
    }

}
