package npm.totebag;

/**
 * Created by deny on 5/22/16.
 */
public class Item_object {
    private int id;
    private String pictItem;
    private String title;
    private String dateTime;
    private String price;
    private String nego;
    private String linkItem;

    public Item_object(){
        this.id = -99;
        this.pictItem = "";
        this.title = "";
        this.dateTime = "";
        this.price = "";
        this.nego = "";
        this.linkItem="";
    }

    public Item_object(int id, String pictItem, String title, String dateTime, String price, String nego, String linkItem) {
        this.id = id;
        this.pictItem = pictItem;
        this.title = title;
        this.dateTime = dateTime;
        this.price = price;
        this.nego = nego;
        this.linkItem = linkItem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPictItem() {
        return pictItem;
    }

    public void setPictItem(String pictItem) {
        this.pictItem = pictItem;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNego() {
        return nego;
    }

    public void setNego(String nego) {
        this.nego = nego;
    }

    public String getLinkItem() {
        return linkItem;
    }

    public void setLinkItem(String linkItem) {
        this.linkItem = linkItem;
    }
}
