package npm.totebag;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import SQLite.BookmarkHelper;

/**
 * Created by deny on 5/22/16.
 */
public class ItemAdapter extends BaseAdapter {
    private Context mcontext;
    private List<Item_object> mItemSearchList;
    private BookmarkHelper SQL;

    public ItemAdapter(Context mcontext, List<Item_object> mItemSearchList) {
        this.mcontext = mcontext;
        this.mItemSearchList = mItemSearchList;
        this.SQL = new BookmarkHelper(mcontext);
    }

    public int getCount() {
        return mItemSearchList.size();
    }


    public Object getItem(int position) {
        return mItemSearchList.get(position);
    }


    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = View.inflate(mcontext, R.layout.row_layout_list_item_search, null);

        ImageView ivItem = (ImageView) v.findViewById(R.id.pictItem);
        TextView tvTitle = (TextView) v.findViewById(R.id.titleItem);
        ImageView iconSourceS = (ImageView) v.findViewById(R.id.icon_source_s);
        TextView tvDateTime = (TextView) v.findViewById(R.id.timeItem);
        TextView tvPrice = (TextView) v.findViewById(R.id.priceItem);
        TextView tvNego = (TextView) v.findViewById(R.id.negoPriceItem);
        final ImageButton addBookmark = (ImageButton) v.findViewById(R.id.addToBookmark);

        Picasso.with(mcontext).load(mItemSearchList.get(position).getPictItem()).into(ivItem);
        tvTitle.setText(mItemSearchList.get(position).getTitle());
        iconSourceS.setImageResource(R.drawable.logo_olx);
        tvDateTime.setText(mItemSearchList.get(position).getDateTime());
        tvPrice.setText(mItemSearchList.get(position).getPrice());
        tvNego.setText(mItemSearchList.get(position).getNego());
        addBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQL.insert(mItemSearchList.get(position));
                addBookmark.setImageResource(R.drawable.ic_bookmark_yellow);
                Toast.makeText(mcontext, "add to bookmark", Toast.LENGTH_SHORT).show();
            }
        });

        v.setTag(mItemSearchList.get(position).getTitle());



        return v;
    }
}