package SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

import npm.totebag.Item_object;

/**
 * Created by andrian on 5/30/16.
 */
public class BookmarkHelper extends SQLiteOpenHelper{

    private int id;
    private String pictItem;
    private String title;
    private String dateTime;
    private String price;
    private String nego;
    private String linkItem;

    public static final String DATABASE_NAME = "ToteBag.db";
    public static final String BOOKMARKS_TABLE_NAME = "bookmarks";
    public static final String BOOKMARKS_COLUMN_ID = "id";
    public static final String BOOKMARKS_COLUMN_TITLE = "title";
    public static final String BOOKMARKS_COLUMN_IMAGE = "image";
    public static final String BOOKMARKS_COLUMN_DATE = "datetime";
    public static final String BOOKMARKS_COLUMN_PRICE = "price";
    public static final String BOOKMARKS_COLUMN_LINK = "link";
    private HashMap hp;

    public BookmarkHelper(Context context){
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table " + BOOKMARKS_TABLE_NAME + " " +
                        "(" + BOOKMARKS_COLUMN_ID + " integer primary key, " +
                        BOOKMARKS_COLUMN_TITLE + " text," +
                        BOOKMARKS_COLUMN_IMAGE + " text," +
                        BOOKMARKS_COLUMN_DATE + " text," +
                        BOOKMARKS_COLUMN_PRICE + " text," +
                        BOOKMARKS_COLUMN_LINK + " text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + BOOKMARKS_TABLE_NAME);
        onCreate(db);
    }

    public boolean insert(Item_object item){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(BOOKMARKS_COLUMN_TITLE, item.getTitle());
        contentValues.put(BOOKMARKS_COLUMN_IMAGE, item.getPictItem());
        contentValues.put(BOOKMARKS_COLUMN_DATE, item.getDateTime());
        contentValues.put(BOOKMARKS_COLUMN_PRICE, item.getPrice());
        contentValues.put(BOOKMARKS_COLUMN_LINK, item.getLinkItem());
        db.insert(BOOKMARKS_TABLE_NAME, null, contentValues);

        return true;
    }


    public Cursor getData(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+BOOKMARKS_TABLE_NAME+" where id="+id+"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, BOOKMARKS_TABLE_NAME);
        return numRows;
    }

    public boolean updateBookmark (Integer id, Item_object item)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BOOKMARKS_COLUMN_ID, item.getId());
        contentValues.put(BOOKMARKS_COLUMN_TITLE, item.getTitle());
        contentValues.put(BOOKMARKS_COLUMN_IMAGE, item.getPictItem());
        contentValues.put(BOOKMARKS_COLUMN_DATE, item.getDateTime());
        contentValues.put(BOOKMARKS_COLUMN_PRICE, item.getPrice());
        contentValues.put(BOOKMARKS_COLUMN_LINK, item.getLinkItem());
        db.update(BOOKMARKS_TABLE_NAME, contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }


    public Integer deleteBookmark (Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(BOOKMARKS_TABLE_NAME,
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    public ArrayList<Item_object> getAllBookmarks()
    {
        ArrayList<Item_object> array_list = new ArrayList<Item_object>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+BOOKMARKS_TABLE_NAME, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            Item_object item = new Item_object();
            item.setId(res.getInt(res.getColumnIndex(BOOKMARKS_COLUMN_ID)));
            item.setTitle(res.getString(res.getColumnIndex(BOOKMARKS_COLUMN_TITLE)));
            item.setPictItem(res.getString(res.getColumnIndex(BOOKMARKS_COLUMN_IMAGE)));
            item.setDateTime(res.getString(res.getColumnIndex(BOOKMARKS_COLUMN_DATE)));
            item.setPrice(res.getString(res.getColumnIndex(BOOKMARKS_COLUMN_PRICE)));
            item.setLinkItem(res.getString(res.getColumnIndex(BOOKMARKS_COLUMN_LINK)));

            array_list.add(item);
            res.moveToNext();
        }
        return array_list;
    }
}
